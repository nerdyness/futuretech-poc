# FutureTech Payments POC
## About
"FutureTech" is a made up company for a mock-project to explore some [AWS Services](https://aws.amazon.com/) which I had't used before.

This customer has a canteen with subsidised, fix-priced food that their employees love. However, at lunch time the queues at the registers can get pretty long and they came to us to find a better solution for them. The below architecture diagram is what we came up with.
## Architecture
![AWS Architecture](/MB3.png)
### Training Workflow
The architecture is separated into two distinct workflows. The first one is Lambda function that trains the Amazon Rekognition Service with photos from the employees staff badges. It's based on a (pre-existing) AWS Directory Service and a VPC Connected AWS Lambda. The Lambda connects to the Managed AWS Directory and retrieves the stored staff photos for all staff members. Then it trains an Amazon Rekognition Collection with the images found in the AWS Directory.

### Searching Workflow
The second workflow is a camera operated Point of Sales system which the customer will have to install on premise. This system takes a photo of the staff member at the register and sends it to Amazon Rekognition for analysis. Amazon Rekognition will compare the photo to the collection and identify one of 3 things:
1. A highly-confident match upon which the employee will be charged.
2. A face but that face isn't part of the collection, at which point the user is prompted to try again.
3. Too many (>1) or not enough faces (<1), at which point the user is prompted to try again.

The user will be charged by adding the charge to the payroll system via an API call. To facilitate this, every transaction is first put on an SQS queue and a time-based Lambda (Lambda Poll) will poll the SQS queue and process the message accordingly.

## Set up
* The POC expects the following two parameters set up in the Parameter Store in the AWS Systems Manager (SSM). These need to contain a valid Active Directory user that is allowed to traverse all Users in the Active Directory and read the attribute "thumbnailphoto".

1. **/futuretech-payments/prod/ad-user**
2. **/futuretech-payments/prod/ad-password**

* To deploy the solution you need to issue the following commands from the [./backend](./backend) directory:

1. `make layer deploy`
2. Copy the API endpoint from the output and update the frontend/dot\_env file.
3. Copy frontend/dot\_env to frontend/.env
4. Run the training Lambda to set up the Rekognition Collection.
5. Run `make serve` in the frontend/ directory.

## Test
Browse to http://localhost:8080

### Development work
In it's earliest form, this was written using [AWS SAM CLI](https://github.com/awslabs/aws-sam-cli) and the old, out-of-date template is still available [here](./backend/template.yaml). This has now been updated to use the [AWS CDK](https://aws.amazon.com/cdk/) and also got a frontend in [Vue.js](https://vuejs.org/).

Prior to the JavaScript frontend this was all done using [Ruby](https://www.ruby-lang.org/en/) and the code of that still lives in [./local](./local) for development purposes.
1. `cd local; bundle install` - Will download all the dependencies required for this.
2. `rake take` - Will take a photo (with the webcam of your laptop) and send it to Amazon Rekognition for comparison.

### Dependencies
* [AWS CDK](https://aws.amazon.com/cdk/)
* [Docker](https://www.docker.com/)
* [Ruby](https://www.ruby-lang.org/en/) 2.5.0
* [GNU/Make](https://www.gnu.org/software/make/)
* [Bash](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29)
* [Zip](https://www.7-zip.org/)
