# FutureTech Payment POC Backend

This is written using the [AWS CDK in TypeScript](https://docs.aws.amazon.com/cdk/api/latest/)/.

The [cdk.json](./cdk.json) file tells the CDK Toolkit how to execute your app.

## Useful commands

 * `make`
```
$ make
help       Display this help text
bootstrap  Install the CDK Toolkit (needed for the CDK)
watch      Check for errors as you're editing CDK's typscript
synth      Print out the CloudFormation code for this app
deploy     Deploy the CDK Stack
diff       Show the differences between what is local and what is deployed
docs       Attempt to open the CDK documentation in a browser
destroy    Destroy the CDK Stack
layer      Build the Lambda Layer
```

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
