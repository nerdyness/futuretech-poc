#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { FutureTechBackend } from '../lib/futuretech-stack';

const app = new cdk.App();
new FutureTechBackend(app, 'FutureTechBackend');
