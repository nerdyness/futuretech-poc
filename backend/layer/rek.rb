require 'aws-sdk-rekognition'
require 'json'

class Rek
  def initialize
    # FIXME: Make this a real library w/ optional arguments
    # FIXME: Exception handling anyone? :-D
    @client = Aws::Rekognition::Client.new()
    @collection = "futuretech-staff-badges"
    @bucket = 'my-eu-west-1-images'
  end

  def print(hash)
    puts JSON.pretty_generate(hash)
    hash
  end

  # Creates the collection for storing image data if it doesn't exist yet.
  def create
    options = { collection_id: @collection }
    begin
      @client.describe_collection(options)
    rescue Aws::Rekognition::Errors::ResourceNotFoundException => e
      # Resource not found, create it!
      resp = @client.create_collection({
        collection_id: @collection,
      })
    rescue => e
      # Any other exception (AccessDenied etc), print it out for debugging purposes.
      p e
      STDERR.puts e
    end
    print(resp.to_h)
  end

  def list_collections
    puts "Fetching all collections..."
    resp = @client.list_collections.to_h
    print(resp)
  end

  def describe_collection
    puts "Describing #{@collection} collection..."
    resp = @client.describe_collection({
      collection_id: @collection,
    })
    print(resp.to_h)
  end

  def index_face(refernce_face, name)
    puts "Indexing #{name}"
    resp = @client.index_faces({
      collection_id: @collection,
      detection_attributes: [
      ],
      external_image_id: name,
      image: {
        bytes: refernce_face,
      },
    })
    print(resp.to_h)
  end

  def search(bytes)
    puts "Find face in collection"
    begin
      resp = @client.search_faces_by_image({
        collection_id: @collection,
        face_match_threshold: 95,
        image: {
          bytes: bytes,
        },
        max_faces: 5,
      })
    rescue Aws::Rekognition::Errors::InvalidParameterException => e
      STDERR.puts e
    end
    print(resp.to_h)
  end

  def search_s3(testImage)
    puts "Compare with test image #{testImage}"
    resp = @client.search_faces_by_image({
      collection_id: @collection,
      face_match_threshold: 95,
      image: {
        s3_object: {
          bucket: @bucket,
          name: testImage,
        },
      },
      max_faces: 5,
    })
    print(resp.to_h)
  end

  # Errors returned from Amazon Rekognition all extend Errors::ServiceError.
  # begin
  # # do stuff
  # rescue Aws::Rekognition::Errors::ServiceError
  #   # rescues all service API errors
  #  end
end
