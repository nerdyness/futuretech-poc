require 'aws-sdk-ssm'

class Shared
  def fetch_parameter(name)
    client = Aws::SSM::Client.new
    resp = client.get_parameter({
      name: name,
      with_decryption: true
    })
    resp["parameter"]["value"]
  end
end
