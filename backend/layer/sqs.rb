require 'aws-sdk-sqs'

class SQS
	def initialize(queue_url)
		@queue_url = queue_url
		@client = Aws::SQS::Client.new
	end

	def poll(opts = nil)
		poller = Aws::SQS::QueuePoller.new(@queue_url)
		poller.poll(idle_timeout: 1, wait_time_seconds: 1) do |msg|
			begin
				puts "Sending this to payment API"
				puts msg.body
			rescue => e
				STDERR.puts e
				throw :skip_delete
			end
		end
	end

	def publish(body)
		puts "Publishing message"
		@client.send_message({
			queue_url: @queue_url,
			message_body: body.to_s
		})
	end
end
