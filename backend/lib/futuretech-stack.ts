import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as sqs from '@aws-cdk/aws-sqs';

/** Import the Lambdas */
import { Search } from './search';
import { Poll } from './poll';
import { Train } from './train';

export class FutureTechBackend extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    /** My SQS queues */
    const dlq = new sqs.Queue(this, 'DLQ', {
			queueName: "PaymentsDeadLetterQueue"
    });
    const paymentsQueue = new sqs.Queue(this, 'PaymentsQueue', {
			deadLetterQueue: {
        queue: dlq,
        maxReceiveCount: 3
      },
			queueName: "PaymentsQueue"
    });

    /** Rekognition Collection */
    const rekognitionCollection = this.formatArn({
        service: "rekognition",
        resource: "collection",
        resourceName: "futuretech-staff-badges"
    })

    /** Lambda Layer */
    const layer = new lambda.LayerVersion(this, 'SharedLayer', {
        code: lambda.Code.fromAsset('layer-built'),
        compatibleRuntimes: [lambda.Runtime.RUBY_2_5],
        license: 'GPL-2.0',
        description: 'A shared layer',
    });

    /** Lambda Functions */
    const search = new Search(this, 'SearchLambda', {
      layer: layer,
      paymentsQueue: paymentsQueue,
      collection: rekognitionCollection
    });

    const poll = new Poll(this, 'PollLambda', {
      layer: layer,
      paymentsQueue: paymentsQueue
    });

    const train = new Train(this, 'TrainLambda', { layer: layer });
  }
}

