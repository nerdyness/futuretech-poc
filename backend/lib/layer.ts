import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';

export class Layer extends cdk.Construct {
  /** allows accessing the Lambda Layer */
  public readonly layer: lambda.LayerVersion;

  constructor(scope: cdk.Construct, id: string, props: {}) {
    super(scope, id);

    const layer = new lambda.LayerVersion(this, 'SharedLayer', {
        code: lambda.Code.fromAsset('layer-built'),
        compatibleRuntimes: [lambda.Runtime.RUBY_2_5],
        license: 'GPL-2.0',
        description: 'A shared layer',
    });
  }
}
