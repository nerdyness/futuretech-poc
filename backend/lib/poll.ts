import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as sqs from '@aws-cdk/aws-sqs';
import events = require('@aws-cdk/aws-events');
import targets = require('@aws-cdk/aws-events-targets');

export interface PollProps {
  layer: lambda.LayerVersion,
  paymentsQueue: sqs.Queue
}

export class Poll extends cdk.Construct {
  /** allows accessing the Lambda function handler */
  public readonly handler: lambda.Function;

  constructor(scope: cdk.Construct, id: string, props: PollProps) {
    super(scope, id);

    // Poll Lambda
    const pollLambda = new lambda.Function(this, 'poll', {
      code: lambda.Code.fromAsset('poll'),
      handler: 'app.lambda_handler',
      runtime: lambda.Runtime.RUBY_2_5,
      layers: [ props.layer ],
      environment: {
        PaymentsQueue: props.paymentsQueue.queueUrl
      }
    });

    props.paymentsQueue.grantConsumeMessages(pollLambda);

    const rule = new events.Rule(this, 'Rule', {
      schedule: events.Schedule.expression('cron(0 21 * * ? *)')
    });

    rule.addTarget(new targets.LambdaFunction(pollLambda));
  }
}

