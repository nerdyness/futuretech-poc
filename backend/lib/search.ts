import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as sqs from '@aws-cdk/aws-sqs';
import * as apigw from '@aws-cdk/aws-apigateway';
import iam = require('@aws-cdk/aws-iam');

export interface SearchProps {
  layer: lambda.LayerVersion,
  paymentsQueue: sqs.Queue,
  collection: string
}

export class Search extends cdk.Construct {
  /** allows accessing the Lambda function handler */
  public readonly handler: lambda.Function;

  constructor(scope: cdk.Construct, id: string, props: SearchProps) {
    super(scope, id);

    // Search Lambda
    const searchLambda = new lambda.Function(this, 'search', {
      code: lambda.Code.fromAsset('search'),
      handler: 'app.lambda_handler',
      runtime: lambda.Runtime.RUBY_2_5,
      layers: [ props.layer ],
      environment: {
        PaymentsQueue: props.paymentsQueue.queueUrl
      }
    });

    // Search Lambda Policy
    searchLambda.addToRolePolicy(new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: [props.collection],
        actions: [
          "rekognition:SearchFaces",
          "rekognition:SearchFacesByImage",
          "rekognition:DescribeCollection",
          "rekognition:CreateCollection",
        ]
    }));
    searchLambda.addToRolePolicy(new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: [props.paymentsQueue.queueArn],
        actions: [ "sqs:SendMessage" ]
    }));


    // The API Gateway for it
    const api = new apigw.LambdaRestApi(this, 'FutureTechPoc', {
      handler: searchLambda,
      restApiName: 'future-tech-poc',
      deployOptions: { stageName: 'prod' },
      defaultMethodOptions: {
        apiKeyRequired: false
      },
      proxy: false // Does not use proxy integration!
    });

    const resource = api.root.addResource('search'); // creating a resource
    addCorsOptions(resource); // adding OPTION for CORS

    const search_integration = new apigw.LambdaIntegration(searchLambda);

    const method = resource.addMethod('POST', search_integration, {});
  }
}

export function addCorsOptions(apiResource: apigw.IResource) {
  apiResource.addMethod('OPTIONS', new apigw.MockIntegration({
    integrationResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent'",
        'method.response.header.Access-Control-Allow-Origin': "'*'",
        'method.response.header.Access-Control-Allow-Credentials': "'false'",
        'method.response.header.Access-Control-Allow-Methods': "'OPTIONS,GET,PUT,POST,DELETE'",
      },
    }],
    passthroughBehavior: apigw.PassthroughBehavior.NEVER,
    requestTemplates: {
      "application/json": "{\"statusCode\": 200}"
    },
  }), {
    methodResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': true,
        'method.response.header.Access-Control-Allow-Methods': true,
        'method.response.header.Access-Control-Allow-Credentials': true,
        'method.response.header.Access-Control-Allow-Origin': true,
      },
    }]
  })
}

