import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';

export interface TrainProps {
    layer: lambda.LayerVersion;
}

export class Train extends cdk.Construct {
  /** allows accessing the Lambda function handler */
  public readonly handler: lambda.Function;

  constructor(scope: cdk.Construct, id: string, props: TrainProps) {
    super(scope, id);

    // Train Lambda
    const trainLambda = new lambda.Function(this, 'train', {
      code: lambda.Code.fromAsset('train'),
      handler: 'app.lambda_handler',
      runtime: lambda.Runtime.RUBY_2_5,
      layers: [ props.layer ]
      // memorySize: 1024
    });
  }
}

