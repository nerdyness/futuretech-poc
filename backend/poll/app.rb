require 'sqs'

def lambda_handler(event:, context:)
  sqs = SQS.new(ENV['PaymentsQueue'])
  opts = {
    idle_timeout: 1,
    wait_time_seconds: 1
  }
  sqs.poll(opts)
end
