$LOAD_PATH << File.expand_path(File.dirname(__FILE__))

require 'json'
require 'rek'
require 'base64'
require 'sqs'
require 'uri'

def recognise(bytes)
  rek = Rek.new
  rek.create # Will create the collection if it doesn't exist
  # Return response from Rekognition
  rek.search(bytes)
end

def place_on_queue(message)
  sqs = SQS.new(ENV['PaymentsQueue'])
  sqs.publish(message)
end

def cors_web_response(status_code, body)
  {
    statusCode: status_code,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
      # 'Access-Control-Allow-Methods': 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT',
      'Access-Control-Allow-Methods': 'OPTIONS,POST',
      'Access-Control-Allow-Origin': '*'
    },
    'body': body.to_json
  }
end

def not_recognised(message)
  cors_web_response(200, {
    message: 'Did not recognise this person, please try again.',
    details: message.to_h
  })
end

def recognised(message)
  face_match = message[:face_matches].first
  matched = face_match[:face]
  # Return positive feedback to client
  cors_web_response(200, {
    message: "Recognised #{matched[:external_image_id]} with #{matched[:confidence]}% confidence",
    details: message.to_h
  })
end

def lambda_handler(event:, context:)
  # FIXME: Dodgy work around
  # The javascript frontend sends something like body=data:jpeg/base64,[data] URL encoded
  begin
    body = event['body'].sub('body=', '')
    body = URI.decode(body).chomp
    tmp = body.split(',')[1]
    if tmp.nil?
      bytes = Base64.decode64(body)
    else
      bytes = Base64.decode64(tmp)
    end

    # Actual logic...
    rek_resp = recognise(bytes)

    # Did we recognise 1 face ?
    if rek_resp.key?(:face_matches) && rek_resp[:face_matches].size == 1
      place_on_queue(rek_resp)  # Put message on SQS
      return recognised(rek_resp)
    end
    # Face not recognised.
    not_recognised(rek_resp)
  rescue => e
    p e.message
    p e.inspect
    # Return something
    cors_web_response(500, {
      message: 'Something terrible has happened',
      details: "#{e.message} #{e.inspect}"
    })
  end
end
