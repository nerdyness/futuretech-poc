require 'base64'
require 'json'
require 'net/ldap'
require 'rek'
require 'shared'

def lambda_handler(event:, context:)
  # Create rek object from library in Lambda Layer
  rek = Rek.new
  rek.create # Will create the collection if it doesn't exist

  # Create shared object from library in Lambda Layer
  shared = Shared.new
  ad_user = shared.fetch_parameter('/futuretech-payments/prod/ad-user')
  ad_password = shared.fetch_parameter('/futuretech-payments/prod/ad-password')

  # Create ldap object from dependency in Lambda Layer
  ldap = Net::LDAP.new({:host => '172.31.51.22',
                        :port => 389,
                        :auth => {
                          :method => :simple,
                          :username => ad_user,
                          :password => ad_password
                        }
  })

  treebase = "ou=users,ou=futuretech,dc=future,dc=tech"
  ldap.search(:base => treebase) do |entry|
    puts "DN: #{entry.dn}"
    next if entry[:thumbnailphoto] == []
    pic = entry[:thumbnailphoto].first
    name = entry[:samaccountname].first
    rek.index_face(pic, name)
  end

  p ldap.get_operation_result

  {
    statusCode: 200,
    body: {
      message: "Successfully trained the Rekognition Collection.",
    }.to_json
  }
end
