# FutureTech Payment POC Frontend

## About
This app is written in [Vue.js](https://vuejs.org/).

It can take a picture with the first camera it finds and send it to the API endpoint defined in your [.env](./dot_env) file.

## Useful commands

 * make
```
$ make
help         Displays this help text
dependencies Installs/updates all dependencies
serve        Serves your App locally and rebuilds when changes are detected
lint         Lints your App
build        Builds a deployable version in ./dist
```
