#!/usr/bin/env ruby

require 'av_capture'

class Cam
  def take_picture
    # Create a recording session
    session = AVCapture::Session.new

    # Find the first video capable device
    dev = AVCapture.devices.find(&:video?)

    # Output the camera's name
    puts "Turning on camera: '#{dev.name}'"
    puts "Taking picture in 1 second...."

    # Connect the camera to the recording session
    session.run_with(dev) do |connection|
      # Let the camera turn on properly, otherwise the picture is very dark
      sleep(1)
      # Capture an image and write it to $stdout
      return connection.capture
    end
  end
end
