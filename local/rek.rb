require 'aws-sdk-rekognition'
require 'json'

# This operation creates a Rekognition collection for storing image data.

class Rek
  def initialize
    @client = Aws::Rekognition::Client.new()
    @collection = "futuretech-staff-badges"
    @bucket = 'eu-central-1-artifacts'
  end

  def print(hash)
    puts JSON.pretty_generate(hash)
  end

  def create
    resp = @client.create_collection({
      collection_id: @collection,
    })
    print(resp.to_h)
  end

  def list_collections
    puts "Fetching all collections..."
    print(@client.list_collections.to_h)
  end

  def describe_collection
    puts "Describing #{@collection} collection..."
    resp = @client.describe_collection({
      collection_id: @collection,
    })
    print(resp.to_h)
  end

#  def index_face(refernce_face, name)
#    puts "Indexing #{name}"
#    resp = @client.index_faces({
#      collection_id: @collection,
#      detection_attributes: [
#      ],
#      external_image_id: name,
#      image: {
#        bytes: refernce_face,
#      },
#    })
#    print(resp.to_h)
#  end

  def index_face(refernce_face)
    puts "Indexing #{refernce_face}"
    resp = @client.index_faces({
      collection_id: @collection,
      detection_attributes: [
      ],
      external_image_id: refernce_face,
      image: {
        s3_object: {
          bucket: @bucket,
          name: refernce_face,
        },
      },
    })
    print(resp.to_h)
  end

  def search_file(bytes)
    puts "Compare with local image"
    resp = @client.search_faces_by_image({
      collection_id: @collection,
      face_match_threshold: 95,
      image: {
        bytes: bytes,
      },
      max_faces: 5,
    })
    print(resp.to_h)
  end

  def search(testImage)
    puts "Compare with test image #{testImage}"
    resp = @client.search_faces_by_image({
      collection_id: @collection,
      face_match_threshold: 95,
      image: {
        s3_object: {
          bucket: @bucket,
          name: testImage,
        },
      },
      max_faces: 5,
    })
    print(resp.to_h)
  end


  # Errors returned from Amazon Rekognition all extend Errors::ServiceError.
  # begin
  # # do stuff
  # rescue Aws::Rekognition::Errors::ServiceError
  #   # rescues all service API errors
  #  end
end

